package main;

import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        System.out.println("1 "+Pattern.matches("[a-zа-я]+","ырырdhhsfgh")); //нижний регистр
        System.out.println("2 "+Pattern.matches("[A-ZА-Я]+","ырырdhhsfgh")); //верхний регистр
        System.out.println("3 "+Pattern.matches("[A-ZА-Я]+","ПЫРЫАПHGSSDH"));
        System.out.println("4 "+Pattern.matches("[A-ZА-Я]+","ырырdhhsfgh"));
        System.out.println("5 "+Pattern.matches("[abc]+","ырырdhhsfgh"));  // только abc
        System.out.println("6 "+Pattern.matches("[abc]+","abcbcab"));
        System.out.println("7 "+Pattern.matches("[yxz]+","ырырdhhsfgh")); // только yxz
        System.out.println("8 "+Pattern.matches("[yxz]+","yzxyxxxyyz"));
        System.out.println("9 "+Pattern.matches("^(987).+","ырырdhhsfgh")); // начиная с 987
        System.out.println("10 "+Pattern.matches("^(987).+","987564235"));
        System.out.println("11 "+Pattern.matches("[A-Za-zА-Яа-я0-9]{0,5}","ыры")); // не больше 5
        System.out.println("12 "+Pattern.matches("[A-Za-zА-Яа-я0-9]{0,5}","ырырdhhsfgh"));
        System.out.println("13 "+Pattern.matches("[A-Za-zА-Яа-я0-9]{5,}","ырырdhhsfgh")); // не меньше 5
        System.out.println("14 "+Pattern.matches("[A-Za-zА-Яа-я0-9]{5,}","ырыр"));
        System.out.println("15 "+Pattern.matches("(([0-9]{1,}).+|([a-z]{1,}).+)(([0-9]{1,}).+|([a-z]{1,}).+)@{1}+([a-z0-9]{1,}).+(\\.com)${10,}","samplemail123@mail.com"));
        System.out.println("16 "+Pattern.matches("(([0-9]{1,}).+|([a-z]{1,}).+)(([0-9]{1,}).+|([a-z]{1,}).+)@{1}+([a-z0-9]{1,}).+(\\.com)${10,}","1q@mail.com"));
        System.out.println("17 "+Pattern.matches("[^0-9]+","sdAAgdfGgdSFsgf"));
        System.out.println("18 "+Pattern.matches("[^0-9]+","aadfhgasfdh2185fsdfa"));
    }
}
